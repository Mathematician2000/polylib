module Poly
    ( Polynomial(..)

    , readZPoly
    , readRPoly

    , negPoly
    , addPoly
    , subPoly
    , evalPoly
    , degPoly

    , multPoly
    , powPoly

    , divPoly
    , remPoly
    , gcdPoly

    , intMonom
    , realMonom

    , mkZPoly
    , mkRPoly
    ) where


import Data.Complex(Complex)
import Prelude hiding ((^))
import qualified Prelude as P

import BasePoly
    ( Coef
    , IntCoef
    , Polynomial(..)

    , readZPoly
    , readRPoly

    , dropTrailZeros
    , negPoly
    , addPoly
    , subPoly
    , evalPoly
    , degPoly

    , intMonom
    , realMonom

    , mkZPoly
    , mkRPoly
    )
import FFT
    ( fft

    , fromZtoC
    , fromRtoC

    , fromCtoZ
    , fromCtoR
    )
import Utils
    ( _EPS
    , binpow
    )


instance (Num a) => Num (Polynomial a) where
    (+) = addPoly
    (-) = subPoly
    (*) = multPoly
    abs = undefined
    signum = undefined
    fromInteger = undefined
    negate = negPoly

instance (Coef a) => Fractional (Polynomial a) where
    fromRational q = RealPoly [fromRational q]
    (/) = divPoly


-- Multiply two polynomials
multPoly :: Polynomial a -> Polynomial a -> Polynomial a
multPoly p1@(RealPoly _) p2@(RealPoly _) = fromCtoR $ (fromRtoC p1) * (fromRtoC p2)
multPoly p1@(IntPoly _) p2@(IntPoly _) = fromCtoZ $ (fromZtoC p1) * (fromZtoC p2)
multPoly (ComplexPoly [x]) (ComplexPoly [y]) = dropTrailZeros $ ComplexPoly [x * y]
multPoly (ComplexPoly p1) (ComplexPoly p2) = dropTrailZeros $ fft res True
  where
    res = ComplexPoly $ zipWith (*) f1 f2
    d1 = length p1
    d2 = length p2
    deg = 2 P.^ (1 + ceiling (logBase 2 $ fromIntegral (max d1 d2)))
    f1 = carr $ fft (ComplexPoly $ p1 ++ replicate (deg - d1) 0) False
    f2 = carr $ fft (ComplexPoly $ p2 ++ replicate (deg - d2) 0) False


-- Exponentiate a polynomial
powPoly :: (Num a, Integral b) => Polynomial a -> b -> Polynomial a
powPoly = binpow

(^) :: (Num a, Integral b) => Polynomial a -> b -> Polynomial a
(^) = powPoly


-- Divide two polynomials (R[x] case)
divPoly :: (Coef a) => Polynomial a -> Polynomial a -> Polynomial a
divPoly (RealPoly [0]) _ = RealPoly [0]
divPoly _ (RealPoly [0]) = error "Zero division error"
divPoly p1@(RealPoly poly1) p2@(RealPoly poly2)
    | d1 < d2 || abs w < _EPS = fromRational 0
    | otherwise = monom + divPoly (p1 - p2 * monom) p2
  where
    d1 = degPoly p1
    d2 = degPoly p2
    h1 = last poly1
    h2 = last poly2
    w = h1 / h2
    monom = realMonom w (d1 - d2)


-- Find the remainder of two polynomials division (R[x] case)
remPoly :: (Coef a) => Polynomial a -> Polynomial a -> Polynomial a
remPoly p1 p2 = p1 - p2 * (divPoly p1 p2)


-- Find greatest common divisor of two polynomials (R[x] case)
gcdPoly :: (Coef a) => Polynomial a -> Polynomial a -> Polynomial a
gcdPoly (RealPoly [0]) (RealPoly [0]) = error "GCD is undefined for two zeroes"
gcdPoly (RealPoly [0]) p2 = p2
gcdPoly p1 (RealPoly [0]) = p1
gcdPoly p1 p2
    | p1 == p2 = p1
    | d1 < d2 = gcdPoly p2 p1
    | otherwise = gcdPoly p2 (remPoly p1 p2)
  where
    d1 = degPoly p1
    d2 = degPoly p2
