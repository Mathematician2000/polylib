{-# LANGUAGE GADTs, TypeOperators #-}

module BasePoly
    ( Coef
    , IntCoef
    , Polynomial(..)

    , readZPoly
    , readRPoly

    , dropTrailZeros
    , negPoly
    , addPoly
    , subPoly
    , evalPoly
    , degPoly

    , intMonom
    , realMonom

    , mkZPoly
    , mkRPoly
    ) where


import Data.Char
    ( isSpace
    , toUpper
    )
import Data.Complex
    ( Complex(..)
    , magnitude
    )
import Data.List(reverse)
import Data.Maybe
    ( Maybe(..)
    , fromJust
    )
import qualified Data.Text as Txt
import Text.Printf
    ( PrintfArg(..)
    , printf
    )
import qualified Text.Scanf as Scn((:+)(..))
import Text.Scanf
    ( Format

    , scanf
    , fmt_

    , int
    , integer
    , double
    )

import Utils
    ( _EPS
    , zipLongest
    , enumerate
    )


class (PrintfArg a, Show a, Read a, RealFloat a) => Coef a
instance Coef Double

class (PrintfArg a, Show a, Read a, Integral a) => IntCoef a
instance IntCoef Integer


-- Polynomial type: R[x], Z[x] or C[x]
data Polynomial a where
    RealPoly :: (Coef a) => {arr :: [a]} -> Polynomial a
    IntPoly :: (IntCoef a) => {arr :: [a]} -> Polynomial a
    ComplexPoly :: (Coef a) => {carr :: [Complex a]} -> Polynomial a

instance Eq (Polynomial a) where
    (RealPoly p1) == (RealPoly p2) = p1 == p2
    (IntPoly p1) == (IntPoly p2) = p1 == p2
    (ComplexPoly p1) == (ComplexPoly p2) = p1 == p2

instance Read (Polynomial a) where
    readsPrec = undefined

instance Show (Polynomial a) where
    show (RealPoly poly) = showPoly (printf "+%f") poly
    show (IntPoly poly) = showPoly (printf "+%d") poly
    show (ComplexPoly poly) = showPoly output poly
      where
        output :: (Coef a) => Complex a -> String
        output (x :+ 0) = printf "+%f" x
        output (0 :+ y) = printf "+%fj" y
        output (x :+ y) = printf "+(%f+%fj)" x y


-- Convert a polynomial to a string
showPoly :: (Eq a, Num a) => (a -> String) -> [a] -> String
showPoly _ [0] = "0"
showPoly output coefs = Txt.unpack txt
  where
    txt = Txt.dropWhile (== '+') $
          Txt.filter (not . isSpace) $
          Txt.replace (Txt.pack "+-") (Txt.pack "-") rawTxt
    rawTxt = Txt.pack $ unwords $ reverse $ map (showMonom output) $ enumerate coefs


-- Convert a monomial to a string
showMonom :: (Eq a, Num a) => (a -> String) -> (Int, a) -> String
showMonom _ (_, 0) = ""
showMonom output (0, w) = output w
showMonom _ (1, 1) = "+X"
showMonom _ (1, -1) = "-X"
showMonom output (1, w) = output w ++ "X"
showMonom _ (n, 1) = "+X^" ++ printf "%d" n
showMonom _ (n, -1) = "-X^" ++ printf "%d" n
showMonom output (n, w) = output w ++ "X^" ++ printf "%d" n


-- Parse a string to a polynomial
readZPoly :: String -> Polynomial Integer
readZPoly "" = IntPoly [0]
readZPoly raw = addPoly (readCoef coef integer intMonom) (readZPoly (drop (length coef) s))
  where
    coef = head s : takeWhile (\c -> c /= '+' && c /= '-') (tail s)
    s = map toUpper $ filter (not . isSpace) raw

readRPoly :: String -> Polynomial Double
readRPoly "" = RealPoly [0]
readRPoly raw = addPoly (readCoef coef double realMonom) (readRPoly (drop (length coef) s))
  where
    coef = head s : takeWhile (\c -> c /= '+' && c /= '-') (tail s)
    s = map toUpper $ filter (not . isSpace) raw

readCoef :: (Num a)
         => String
         -> (Format () -> Format (a Scn.:+ ()))
         -> (a -> Int -> Polynomial a)
         -> Polynomial a
readCoef s format mkMonom
    | n >= 0 = mkMonom w n
    | otherwise = error "Negative exponent in a polynomial"
  where
    w | coef == "" || coef == "+" = 1
      | coef == "-" = -1
      | otherwise = case wScanned of
            (Just (result Scn.:+ ())) -> result
            Nothing -> error $ "Failed to scan monomial '" ++ s ++ "': w = '" ++ show wToScan ++ "'"
    n | null exp = 0
      | exp == "X" = 1
      | otherwise = case nScanned of
            (Just (result Scn.:+ ())) -> result
            Nothing -> error $ "Failed to scan monomial '" ++ s ++ "': n = '" ++ show nToScan ++ "'"
    wScanned = scanf (fmt_ format) wToScan
    nScanned = scanf (fmt_ int) nToScan
    wToScan = dropWhile (== '+') coef
    nToScan = drop 2 exp
    coef = takeWhile (/= 'X') s
    exp = drop (length coef) s


-- Remove trailing zeroes of a polynomial
dropTrailZeros :: (Eq a, Num a) => Polynomial a -> Polynomial a
dropTrailZeros p = case p of
    RealPoly poly -> RealPoly $ getRealCoefs poly
    IntPoly poly -> IntPoly $ getIntCoefs poly
    ComplexPoly poly -> ComplexPoly $ getComplexCoefs poly
  where
    getRealCoefs coefs = if null rest then [0] else rest
      where
        rest = reverse $ dropWhile (\r -> abs r < _EPS) $ reverse coefs
    getIntCoefs coefs = if null rest then [0] else rest
      where
        rest = reverse $ dropWhile (== 0) $ reverse coefs
    getComplexCoefs coefs = if null rest then [0] else rest
      where
        rest = reverse $ dropWhile (\c -> abs (magnitude c) < _EPS) $ reverse coefs


-- Negate a polynomial
negPoly :: (Num a) => Polynomial a -> Polynomial a
negPoly p = case p of
    RealPoly poly -> RealPoly $ getCoefs poly
    IntPoly poly -> IntPoly $ getCoefs poly
    ComplexPoly poly -> ComplexPoly $ getCoefs poly
  where
    getCoefs :: (Num b) => [b] -> [b]
    getCoefs = map negate


-- Add two polynomials
addPoly :: (Num a) => Polynomial a -> Polynomial a -> Polynomial a
addPoly p1 p2 = case (p1, p2) of
    (RealPoly ps1, RealPoly ps2) -> dropTrailZeros $ RealPoly (getSum ps1 ps2)
    (IntPoly ps1, IntPoly ps2) -> dropTrailZeros $ IntPoly (getSum ps1 ps2)
    (ComplexPoly ps1, ComplexPoly ps2) -> dropTrailZeros $ ComplexPoly (getSum ps1 ps2)
  where
    getSum :: (Num b) => [b] -> [b] -> [b]
    getSum = zipLongest 0 0 (+)


-- Subtract two polynomials
subPoly :: (Num a) => Polynomial a -> Polynomial a -> Polynomial a
subPoly p1 p2 = addPoly p1 (negPoly p2)


-- Evaluate a polynomial
evalPoly :: (Num a) => Polynomial a -> a -> a
evalPoly poly x = foldr (\w acc -> acc * x + w) 0 (arr poly)


-- Degree of a polynomial
degPoly :: Polynomial a -> Int
degPoly (ComplexPoly poly) = length poly - 1
degPoly poly = length (arr poly) - 1


-- Build a polynomial w*x^n in Z[x]
intMonom :: (IntCoef a) => a -> Int -> Polynomial a
intMonom w n
    | w == 0 = IntPoly [0]
    | n >= 0 = IntPoly (replicate n 0 ++ [w])
    | otherwise = error "Exponent should be a non-negative integer"

-- Build a polynomial w*x^n in R[x]
realMonom :: (Coef a) => a -> Int -> Polynomial a
realMonom w n
    | w == 0 = RealPoly [0]
    | n >= 0 = RealPoly (replicate n 0 ++ [w])
    | otherwise = error "Exponent should be a non-negative integer"

-- Build an Integer polynomial in Z[x]
mkZPoly :: [Integer] -> Polynomial Integer
mkZPoly = IntPoly


-- Build a Double polynomial in R[x]
mkRPoly :: [Double] -> Polynomial Double
mkRPoly = RealPoly
