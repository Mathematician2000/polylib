module FFT
    ( fft

    , fromZtoC
    , fromRtoC

    , fromCtoZ
    , fromCtoR
    ) where


import Data.Complex
    ( Complex(..)

    , cis
    , realPart
    )

import BasePoly
    ( Coef
    , IntCoef
    , Polynomial(..)
    )
import Utils(binpow)


-- Convert a polynomial to a complex polynomial
fromZtoC :: (IntCoef a) => Polynomial a -> Polynomial Double
fromZtoC (IntPoly poly) = ComplexPoly $ map (\x -> fromIntegral x :+ 0) poly

fromRtoC :: (Coef a) => Polynomial a -> Polynomial a
fromRtoC (RealPoly poly) = ComplexPoly $ map (:+ 0) poly


-- Convert a complex polynomial to a standard polynomial (bool: round to int)
fromCtoZ :: (Coef a, IntCoef b) => Polynomial a -> Polynomial b
fromCtoZ (ComplexPoly poly) = IntPoly $ map (round . realPart) poly

fromCtoR :: (Coef a) => Polynomial a -> Polynomial a
fromCtoR (ComplexPoly poly) = RealPoly $ map realPart poly


-- Split a polynomial into even and odd polynomial parts
splitPoly :: (Show a) => [a] -> ([a], [a])
splitPoly = foldr (\elem (p1, p2) -> (elem : p2, p1)) ([], [])


-- Evaluate a polynomial (C[x]) in a list of points
evalFourier :: (Coef a) => (Int -> Int -> Complex a) -> [Complex a] -> [Complex a]
evalFourier _ [x] = [x]
evalFourier getRoot poly = values
  where
    values = zipWith (+) evens' odds' ++ zipWith (-) evens' odds'
    odds' = zipWith (*) roots $ evalFourier getRoot odds
    evens' = evalFourier getRoot evens
    (evens, odds) = splitPoly poly
    roots = [getRoot k n | k <- [0..(n - 1)]]
    n = length poly


-- Fast Fouried Transform (bool: inverse or not)
fft :: Polynomial a -> Bool -> Polynomial a
fft p@(ComplexPoly [_]) _ = p
fft (ComplexPoly poly) inverse = ComplexPoly $ if inverse then map lambda fp else fp
  where
    getRoot :: (Coef a) => Int -> Int -> Complex a
    getRoot k n = cis $ if inverse then -phi else phi
      where
        phi = 2 * dk * pi / dn
        dk = fromIntegral k
        dn = fromIntegral n

    lambda = (\x -> x / fromIntegral n)
    fp = evalFourier getRoot poly
    n = length poly
