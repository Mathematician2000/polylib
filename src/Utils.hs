module Utils
    ( _EPS
    , binpow
    , modBinpow
    , zipLongest
    , enumerate
    ) where


_EPS :: (RealFloat a) => a
_EPS = 1e-6


-- Binary exponentiation
binpow :: (Num a, Integral b) => a -> b -> a
binpow _ 0 = 1
binpow x 1 = x
binpow x n
    | n > 1 = if odd n then sqr * x else sqr
    | otherwise = error "Exponent should be a non-negative integer"
  where
    root = binpow x (n `div` 2)
    sqr = root * root


-- Modulo binary exponentiation
modBinpow :: Int -> Int -> Int -> Int
modBinpow _ 0 _ = 1
modBinpow x 1 m = x `mod` m
modBinpow x n m
    | n > 1 = (if odd n then sqr * x else sqr) `mod` m
    | otherwise = error "Exponent should be a non-negative integer"
  where
    root = modBinpow x (n `div` 2) m
    sqr = (root * root) `mod` m


-- Zip with default value
zipLongest :: a -> b -> (a -> b -> c) -> [a] -> [b] -> [c]
zipLongest x0 _ f [] ys = zipWith f (cycle [x0]) ys
zipLongest _ y0 f xs [] = zipWith f xs (cycle [y0])
zipLongest x0 y0 f (x : xs) (y : ys) = (f x y : zipLongest x0 y0 f xs ys)


-- Enumerate a list
enumerate :: [a] -> [(Int, a)]
enumerate = zip [0..]
