module Main where


import Data.Complex(Complex(..))
import System.IO
    ( BufferMode(..)

    , hSetBuffering
    , stdout
    , stderr
    )

import FFT
    ( fromZtoC
    , fromRtoC

    , fromCtoZ
    , fromCtoR
    )
import Poly
    ( Polynomial(..)

    , readZPoly
    , readRPoly

    , negPoly
    , addPoly
    , subPoly
    , evalPoly
    , degPoly

    , multPoly
    , powPoly

    , divPoly
    , remPoly
    , gcdPoly

    , intMonom
    , realMonom

    , mkZPoly
    , mkRPoly
    )


newline :: IO ()
newline = putStrLn ""


main :: IO ()
main = do
    hSetBuffering stdout NoBuffering
    hSetBuffering stderr NoBuffering

    newline
    putStrLn "Case #0: String-to-polynomial cast"
    putStrLn $ show $ readZPoly "5-7X^2+6X-3+8X^3+7X^2"
    putStrLn $ show $ readRPoly "5-7X^2+6X-3+8X^3+7X^2"
    putStrLn $ show $ readZPoly "5X^2-4X^2-X^2"
    putStrLn $ show $ readRPoly "-0"
    newline

    putStrLn "Case #1: Z[x] basics"
    putStrLn $ show $ mkZPoly [4, 2, 6]
    putStrLn $ show $ - readZPoly "6X^2+2X+4"
    putStrLn $ show $ readZPoly "6X^2+2X+4" + readZPoly "7X + 3"
    putStrLn $ show $ readZPoly "6X^2+2X+4" + readZPoly "-6X^2 + 5-2X"
    putStrLn $ show $ readZPoly "6X^2+2X+4" - readZPoly "3+6X^2+X"
    newline

    putStrLn "Case #2: R[x] basics"
    putStrLn $ show $ mkRPoly [4, 2, 6]
    putStrLn $ show $ - readRPoly "6X^2+2X+4"
    putStrLn $ show $ readRPoly "6X^2+2X+4" + readRPoly "7X + 3"
    putStrLn $ show $ readRPoly "6X^2+2X+4" + readRPoly "-6X^2 + 5-2X"
    putStrLn $ show $ readRPoly "6X^2+2X+4" - readRPoly "3+6X^2+X"
    newline

    putStrLn "Case #3: Degree & monomials"
    putStrLn $ show $ degPoly (readZPoly "6X^2+2X+4")
    putStrLn $ show $ degPoly (readRPoly "6X^2+2X+4")
    putStrLn $ show $ intMonom (3::Integer) 2 + intMonom 2 5
    putStrLn $ show $ intMonom 3 3 - readZPoly "3+6X^2+X"
    putStrLn $ show $ realMonom (3::Double) 2 + realMonom 2 5
    putStrLn $ show $ realMonom 3 3 - readRPoly "3+6X^2+X"
    newline

    putStrLn "Case #4: Z[x] multiplication"
    putStrLn $ show $ readZPoly "6X + 4" * readZPoly "3"
    putStrLn $ show $ readZPoly " 4" * readZPoly "2X + 3"
    putStrLn $ show $ readZPoly "2X +4" * readZPoly "3+X"
    putStrLn $ show $ readZPoly "3+X"  * readZPoly "3+X"
    putStrLn $ show $ readZPoly "3+X" ^ 2
    newline

    putStrLn "Case #5: R[x] multiplication"
    putStrLn $ show $ readRPoly "6X + 4" * readRPoly "3"
    putStrLn $ show $ readRPoly " 4" * readRPoly "2X + 3"
    putStrLn $ show $ readRPoly "2X +4" * readRPoly "3+X"
    putStrLn $ show $ readRPoly "3+X" * readRPoly "3+X"
    putStrLn $ show $ readRPoly "3+X" ^ 2
    newline

    putStrLn "Case #6: R[x] division & GCD"
    putStrLn $ show $ readRPoly "6X^2+2X+4" / readRPoly "3+6X^2+X"
    putStrLn $ show $ remPoly (readRPoly "6X^2+2X+4") (readRPoly "3+6X^2+X")
    putStrLn $ show $ gcdPoly (readRPoly "6X^2+2X+4") (readRPoly "3+6X^2+X")
    putStrLn $ show $ gcdPoly (readRPoly "6X^2+2X+4" ^ 3) (readRPoly "6X^2+2X+4")
    newline

    putStrLn "Case 7: Polynomial evaluation"
    putStrLn $ show $ evalPoly (readRPoly "6X^2+2X+4") 0
    putStrLn $ show $ evalPoly (readRPoly "6X^2+2X+4") 1
    putStrLn $ show $ evalPoly (readRPoly "6X^2+2X+4") 2
    putStrLn $ show $ evalPoly (readRPoly "6X^2+2X+4") 0.5
    newline

    putStrLn "Case 8: C[x] output + conversions"
    putStrLn $ show $ fromRtoC (readRPoly "6X^2+2x+4")
    putStrLn $ show $ fromZtoC (readZPoly "6X^2+2x+4")
    putStrLn $ show $ fromCtoR (ComplexPoly $ map (:+ 0) [7.5::Double, 4, 0, -1])
    putStrLn $ show $ ((fromCtoZ (ComplexPoly $ map (:+ 0) [7::Double, 4, 0, -1])) :: (Polynomial Integer))
    newline
