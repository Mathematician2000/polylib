# polylib

## A small library to perform simple operations on polynomials

Tiny FFT implementation in Haskell. Main features:
- string operations (read/show) on polynomials
- addition, subtraction, negation and many other simple operations implemented
- efficient polynomial multiplication via FFT
- division, finding a remainder and GCD (using Euclidean algorithm) for R[x] polynomials

Dependencies to be solved:
- scanf
- text

Use ```stack run``` to build and run the application.

Some simple tests are listed in app/Main.hs.
